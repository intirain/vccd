/* vccd/scripts/adduser.c 
 * 
 * This file is part of vccd. 
 * 
 * vccd is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vccd is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vccd. If not, see <https://www.gnu.org/licenses/>
 */ 




#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include <vcc/vcc.h>
#include <vcc/vccd.h>


static const char usage[] = "Usage: adduser username password\n";

static inline int fatal(const char *s) {
	printf("fatal: %s\n", s);

	exit(1);
}

static inline int warn(const char *s) {
	fprintf(stderr, "warning: %s\n", s);

	return 0;
}


/* put $HOME to /etc/vccd-runtime */

int main(int argc, const char **argv) {
	struct user 	usr;
	char 		*runtime, *dir;
	int 		fd;

	memset(&usr, 0, sizeof(struct user));

	if (unlikely(argc != 3)) 
		fatal(usage);

	if (unlikely(!(runtime = malloc(PATH_MAX)))) 
		fatal("malloc() failed. ");

	if (unlikely(!(dir = malloc(PATH_MAX)))) 
		fatal("malloc() failed. ");

	if (unlikely((fd = open("/etc/vccd-runtime", O_RDONLY | O_CREAT)) < 0)) {
		perror(argv[0]);

		return 1;
	}

	read(fd, runtime, PATH_MAX);
	close(fd);

	strncpy(dir, runtime, PATH_MAX);

	/* in install.c, the runtime path always ends with '/' */
	strcat(dir, "users/");
	strcat(dir, argv[1]);

	if (unlikely((fd = open(dir, O_WRONLY | O_CREAT | O_EXCL, 0644)) < 0)) {
		perror(argv[0]);

		return 1;
	}

	usr.score = 0;
	usr.level = MIN_LEVEL;
	strncpy(usr.username, argv[1], USRNAME_SIZE);
	strncpy(usr.passwd, argv[2], PASSWD_SIZE);

	write(fd, &usr, sizeof(struct user));

	close(fd);

	return 0;
}



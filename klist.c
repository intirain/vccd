/* vccd/klist.c 
 * 
 * This file is part of vccd. 
 * 
 * vccd is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vccd is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vccd. If not, see <https://www.gnu.org/licenses/>
 */ 




#include <sys/types.h>
#include <stddef.h>
#include <klist.h>



struct klist_node *klist_find_value(struct klist_node *head, int value) {
	struct klist_node *t;

	for (t = head; t; t = t->next) 
		if (t->value == value) 
			return t;

	return NULL;
}


void klist_init(struct klist_node *head) {
	head->next = head;
	head->prev = head;

	head->value = 0;
	head->data = NULL;
	head->usrname = NULL;

	return;
}


void klist_add(struct klist_node *head, struct klist_node *new) {
	/* head->prev: the last. */
	new->prev = head->prev;
	new->next = head;

	/* head->prev->next: (was) the head */
	head->prev->next = new;
	head->prev = new;

	return;
}


void klist_del(struct klist_node *head, struct klist_node *will_del) {
	(void) head;

	will_del->prev->next = will_del->next;
	will_del->next->prev = will_del->prev;

	return;
}



VCCD_MAJOR 		= 1
VCCD_MINOR 		= 19
VCCD_PATCH 		= 1
VCCD_STR 		= -rc 

VCCD_COMPILE_TIME 	= $(shell date)
VCCD_COMPILE_BY 	= $(shell whoami)
VCCD_SEMVER 		= vccd $(VCCD_MAJOR).$(VCCD_MINOR).$(VCCD_PATCH)$(VCCD_STR)

srcs 			= main.c vcc.c klist.c users.c monitor.c session.c relay.c 
objs 			= main.o vcc.o klist.o users.o monitor.o session.o relay.o 
target 			= vccd
CFLAGS 			= -std=gnu11 -Wall -Wextra -Werror -Iinclude -g 
MAKEFLAGS 		= --no-print-directory

VCCD_VERSION 		= vccd $(VCCD_MAJOR).$(VCCD_MINOR).$(VCCD_PATCH) \
		  $(VCCD_COMPILE_TIME) $(VCCD_COMPILE_BY)

all: 
	@echo "$(VCCD_VERSION)"
	@echo "$(VCCD_SEMVER)"
	@echo "#define VCCD_VERSION \"$(VCCD_VERSION)\"" > include/vcc/version.h 
	@echo "#define VCCD_SEMVER \"$(VCCD_SEMVER)\"" >> include/vcc/version.h
	@$(MAKE) $(target) $(MAKEFLAGS)
	@$(MAKE) -C scripts

$(target): 	$(objs) $(srcs)
	@$(CC) $(CFLAGS) $(objs) -o $(target)
	@echo -e "LD\t\t\t$(target)"

%.o: %.c
	@$(CC) -c $(CFLAGS) $*.c 
	@echo -e "CC\t\t\t$*.c"

.PHONY clean: 
	$(RM) $(objs) $(target) -f 
	@$(MAKE) -C scripts/ clean 


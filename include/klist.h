/* vccd/include/klist.h 
 * 
 * This file is part of vccd. 
 * 
 * vccd is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * (at your option) any later version. 
 * 
 * vccd is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with vccd. If not, see <https://www.gnu.org/licenses/>
 */ 




#ifndef __KLIST_H
#define __KLIST_H

#include <vcc/vcc.h>


/* before, i think using a 'container_of' in things we did in kernel 
 * is silly, but now i realize that, i'm the stupid one, there are 
 * too many things in the structure. */

#define STAT_UNLOGGED 			0
#define STAT_LOGGED 			1

#define check_logged(n) \
	if (find_connection(n)->stat != STAT_LOGGED) { \
		vcc_log("%d requested before logging in. \n", fd); \
		vcc_info("WUNLOG %d\n", fd); \
		return 1; \
	}

struct klist_node {
	long 				value;
	int 				stat;

	union {
		void 			*data;
		int 			nusers;
	};

	union {
		char 			*usrname;
		struct klist_node 	*connection;
	};

	union {
		struct klist_node 	*sending;
		struct klist_node 	*session;
	};

	struct klist_node 		*next, *prev;
};



struct klist_node *klist_find_value(struct klist_node *head, int value);

void klist_init(struct klist_node *head);

void klist_add(struct klist_node *head, struct klist_node *new);
void klist_del(struct klist_node *head, struct klist_node *will_del);

#define klist_empty(list) ((list)->next == (list))

#define KLIST_NODE_INIT(__x) { \
	.value = 0, \
	.data = NULL, \
	.sending = NULL, \
	.usrname = NULL, \
	.next = __x, \
	.prev = __x, \
}


#endif

